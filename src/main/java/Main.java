import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("First word: ");
        String string1 = scanner.next();

        System.out.print("Second word: ");
        String string2 = scanner.next();

        String[] stringArray1=string1.toLowerCase().split("");
        String[] stringArray2=string2.toLowerCase().split("");

        List<String> listString1 = new ArrayList<>();
        List<String> listString2 = new ArrayList<>();

        for(int i=0;i<string1.length();i++){
            listString1.add(stringArray1[i]);
        }

        for(int i=0;i<string2.length();i++){
            listString2.add(stringArray2[i]);
        }

        if(string1.length()!=string2.length()){
            System.out.println();
            System.out.println("Not Anagram");
            System.exit(0);
        }

        // sorting, the hard way.
        String dummy="";
        for(int i=0;i<string1.length();i++){
            for(int j=0;j<string1.length();j++){
                if(j==string1.length()-1){
                    break;
                }
                if(listString1.get(j).compareTo(listString1.get(j+1))>0){
                    dummy=listString1.get(j);
                    listString1.set(j,listString1.get(j+1));
                    listString1.set(j+1,dummy);
                }
            }
        }

        for(int i=0;i<string2.length();i++){
            for(int j=0;j<string2.length();j++){
                if(j==string2.length()-1){
                    break;
                }
                if(listString2.get(j).compareTo(listString2.get(j+1))>0){
                    dummy=listString2.get(j);
                    listString2.set(j,listString2.get(j+1));
                    listString2.set(j+1,dummy);
                }
            }
        }

        for(int i=0;i<listString1.size();i++){
            if(listString1.get(i).compareTo(listString2.get(i)) != 0){
                System.out.println();
                System.out.println("Not Anagram");
                System.exit(0);
            }
        }
        System.out.println();
        System.out.println("Anagram");
    }
}
